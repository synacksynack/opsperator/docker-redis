#!/bin/sh

if test "$DEBUG"; then
    set -x
fi
if test -z "$POD_IP"; then
    POD_IP=$(cat /proc/net/fib_trie | grep "|--"  \
		| egrep -v "(0.0.0.0| 127.|.255$|.0$)" \
		| awk '{print $NF}' | head -1)
fi
if test -z "$REDIS_HOSTNAME"; then
    REDIS_HOSTNAME=`hostname 2>/dev/null`
fi
REDIS_PORT=${REDIS_PORT:-6379}
REDIS_REPLICASET=${REDIS_REPLICASET:-primary}
REDIS_SERVER_ID=`echo "$REDIS_HOSTNAME" | sed 's|^.*-\([0-9][0-9]*\)$|\1|'`
if test "$REDIS_SERVER_ID" -ge 0 2>/dev/null; then
    REDIS_BASENAME=`echo $REDIS_HOSTNAME | sed "s|-$REDIS_SERVER_ID$||"`
else
    REDIS_BASENAME=
fi
SENTINEL_PORT=${SENTINEL_PORT:-26379}
SENTINEL_SVC=${SENTINEL_SVC:-redis-sentinel}
if test "$IS_SENTINEL"; then
    if redis-cli -h 127.0.0.1 -p $SENTINEL_PORT --raw sentinel get-master-addr-by-name $REDIS_REPLICASET; then
	echo Sentinel - alive
    else
	echo Sentinel - dead
	exit 42
    fi
elif test "$REDIS_BASENAME"; then
    if redis-cli -h 127.0.0.1 -p $REDIS_PORT ping; then
	if redis-cli -h 127.0.0.1 -p $SENTINEL_PORT --raw sentinel get-master-addr-by-name $REDIS_REPLICASET | grep "^$POD_IP$" >/dev/null; then
	    echo Master - alive
	else
	    echo Slave - alive
	fi
    else
	echo Dead
	exit 42
    fi
elif redis-cli -h 127.0.0.1 -p $REDIS_PORT ping; then
    echo Redis standalone - alive
else
    echo Redis - dead
    exit 42
fi

exit 0
