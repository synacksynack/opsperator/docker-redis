#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

if ! test -s /etc/redis/redis.conf; then
    if test -z "$REDIS_HOSTNAME"; then
	REDIS_HOSTNAME=`hostname 2>/dev/null`
    fi
    REDIS_DATA_DIR=${REDIS_DATA_DIR:-/var/lib/redis/data}
    REDIS_DOWN_AFTER=${REDIS_DOWN_AFTER:-3}
    REDIS_FAILOVER_TIMEOUT=${REDIS_FAILOVER_TIMEOUT:-60}
    REDIS_PARALLEL_SYNCS=${REDIS_PARALLEL_SYNCS:-1}
    REDIS_PORT=${REDIS_PORT:-6379}
    REDIS_QUORUM=${REDIS_QUORUM:-2}
    REDIS_REPLICASET=${REDIS_REPLICASET:-primary}
    REDIS_SERVER_ID=`echo "$REDIS_HOSTNAME" | sed 's|^.*-\([0-9][0-9]*\)$|\1|'`
    if test "$REDIS_SERVER_ID" -ge 0 2>/dev/null; then
	BASENAME=`echo $REDIS_HOSTNAME | sed "s|-$REDIS_SERVER_ID$||"`
	SENTINEL_PORT=${SENTINEL_PORT:-26379}
	SENTINEL_SVC=${SENTINEL_SVC:-redis}
    else
	BASENAME=
    fi

    cat <<EOF >/etc/redis/redis.conf
    daemonize no
    dir $REDIS_DATA_DIR
    protected-mode no
    bind 0.0.0.0
EOF
    set -- redis-server /etc/redis/redis.conf
    cpt=0
    if test "$BASENAME" -a "$IS_SENTINEL"; then
	echo Waiting for Sentinels to startup ...
	do_reset=false
	while true
	do
	    CURRENT_MASTER=$(redis-cli -h $SENTINEL_SVC -p $SENTINEL_PORT \
		--raw sentinel get-master-addr-by-name $REDIS_REPLICASET \
		2>/dev/null | sed -n 1p)
	    if test "$CURRENT_MASTER"; then
		echo OK
		break
	    elif test "$REDIS_SERVER_ID" = 0 -a "$cpt" -gt 3; then
		#sentinel takes over after 15 sec / 3 retries
		CURRENT_MASTER=$BASENAME-0.$BASENAME
		do_reset=true
		echo Assuming Sentinels to be down, taking over
		break
	    elif test "$cpt" -gt 72; then
		echo Could not reach Sentinel
		exit 1
	    fi
	    sleep 5
	    echo Sentinels are ... down
	    cpt=`expr $cpt + 1`
	done
	if $do_reset; then
	    echo INFO: assuming initial master to be $CURRENT_MASTER:$REDIS_PORT
	    echo INFO: booting as SENTINEL master
	elif test "$CURRENT_MASTER"; then
	    echo INFO: detected initial master $CURRENT_MASTER:$REDIS_PORT
	    echo INFO: booting as SENTINEL slave
	else
	    echo FATAL: failed resolving current master
	    exit 1
	fi
	myip=$(getent hosts $CURRENT_MASTER 2>/dev/null | awk '{print $1}')
	if test "$myip"; then
	    CURRENT_MASTER=$myip
	fi
	cat <<EOF >>/etc/redis/redis.conf
    port $SENTINEL_PORT
    sentinel monitor $REDIS_REPLICASET $CURRENT_MASTER $REDIS_PORT $REDIS_QUORUM
    sentinel down-after-milliseconds $REDIS_REPLICASET ${REDIS_DOWN_AFTER}000
    sentinel failover-timeout $REDIS_REPLICASET ${REDIS_FAILOVER_TIMEOUT}000
    sentinel parallel-syncs $REDIS_REPLICASET $REDIS_PARALLEL_SYNCS
EOF
	set -- $@ --sentinel
    elif test "$BASENAME"; then
	echo Waiting for Sentinels to startup ...
	do_reset=false
	while true
	do
	    CURRENT_MASTER=$(redis-cli -h $SENTINEL_SVC -p $SENTINEL_PORT \
		--raw sentinel get-master-addr-by-name $REDIS_REPLICASET \
		2>/dev/null | sed -n 1p)
	    if test "$CURRENT_MASTER"; then
		echo OK
		break
	    elif test "$REDIS_SERVER_ID" = 0 -a "$cpt" -gt 5; then
		#redis takes over after 5 sec / 5 retries
		do_reset=true
		echo Assuming Sentinels to be down, starting up
		break
	    elif test "$cpt" -gt 360; then
		echo Could not reach Sentinel
		exit 1
	    fi
	    sleep 1
	    echo Sentinels are ... down
	    cpt=`expr $cpt + 1`
	done
	if $do_reset; then
	    echo INFO: could not reach $SENTINEL_SVC:$SENTINEL_PORT
	    echo INFO: booting as MASTER
	elif test "$CURRENT_MASTER"; then
	    myip=$(getent hosts $CURRENT_MASTER 2>/dev/null | awk '{print $1}')
	    if test "$myip"; then
		CURRENT_MASTER=$myip
	    fi
	    echo INFO: assuming master to be $CURRENT_MASTER:$REDIS_PORT
	    echo INFO: booting as SLAVE
	    set -- $@ --slaveof $CURRENT_MASTER $REDIS_PORT
	else
	    echo FATAL: failed resolving current master
	    exit 1
	fi
	echo port $REDIS_PORT >>/etc/redis/redis.conf
    else
	echo port $REDIS_PORT >>/etc/redis/redis.conf
	echo INFO: booting as STANDALONE
    fi
else
    echo NOTICE: re-using existing /etc/redis/redis.conf
fi

if test "$DEBUG"; then
    cat /etc/redis/redis.conf
fi
exec $@
