# k8s Redis

Mix from https://github.com/corybuecker/redis-stateful-set and
https://github.com/docker-library/redis

Start Demo or Master/Slave in OpenShift:

```
$ make ocdemo
$ make ocprod
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name          |    Description                          | Default               |
| :------------------------ | --------------------------------------- | --------------------- |
|  `IS_SENTINEL`            | Starts Sentinel instead of Redis        | undef                 |
|  `REDIS_DATA_DIR`         | Redis Data Directory                    | `/var/lib/redis/data` |
|  `REDIS_DOWN_AFTER`       | Redis Sentinel Mark Down After          | `3` seconds           |
|  `REDIS_FAILOVER_TIMEOUT` | Redis Sentinel Failover Timeout         | `60` seconds          |
|  `REDIS_PARALLEL_SYNCS`   | Redis Parallel Syncs                    | `1` sync              |
|  `REDIS_PORT`             | Redis Port                              | `6379`                |
|  `REDIS_QUORUM`           | Redis Master/Slave Quorum               | `2`                   |
|  `REDIS_REPLICASET`       | Redis Master/Slave Replica Set Name     | `primary`             |
|  `SENTINEL_PORT`          | Redis Port                              | `26379`               |
|  `SENTINEL_SVC`           | Sentinel Service Name                   | `redis-sentinel`      |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point     | Description           |
| :---------------------- | --------------------- |
|  `/var/lib/redis/data`  | Redis Data Directory  |
