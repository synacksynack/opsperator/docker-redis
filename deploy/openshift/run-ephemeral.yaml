apiVersion: v1
kind: Template
labels:
  app: redis
  template: redis-ephemeral
metadata:
  annotations:
    description: Redis database - ephemeral
    iconClass: icon-redis
    openshift.io/display-name: Redis
    tags: redis
  name: redis-ephemeral
objects:
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    labels:
      name: redis-${FRONTNAME}
    name: redis-${FRONTNAME}
  spec:
    replicas: 1
    selector:
      name: redis-${FRONTNAME}
    strategy:
      type: Recreate
    template:
      metadata:
        labels:
          name: redis-${FRONTNAME}
      spec:
        dnsPolicy: ClusterFirst
        containers:
        - imagePullPolicy: IfNotPresent
          livenessProbe:
            failureThreshold: 20
            initialDelaySeconds: 20
            periodSeconds: 10
            timeoutSeconds: 5
            tcpSocket:
              port: 6379
          name: redis
          ports:
          - name: redis
            containerPort: 6379
          readinessProbe:
            exec:
              command:
              - /bin/sh
              - "-i"
              - "-c"
              - /is-ready.sh
            initialDelaySeconds: 10
            periodSeconds: 5
            timeoutSeconds: 3
          resources:
            limits:
              cpu: "${REDIS_CPU_LIMIT}"
              memory: "${REDIS_MEMORY_LIMIT}"
          volumeMounts:
          - name: data
            mountPath: /var/lib/redis/data
        restartPolicy: Always
        volumes:
        - emptyDir: {}
          name: data
    triggers:
    - type: ImageChange
      imageChangeParams:
        automatic: true
        containerNames:
        - redis
        from:
          kind: ImageStreamTag
          name: redis:${REDIS_IMAGE_TAG}
    - type: ConfigChange
- apiVersion: v1
  kind: Service
  metadata:
    name: redis-${FRONTNAME}
  spec:
    ports:
    - name: redis
      protocol: TCP
      port: 6379
      targetPort: 6379
    selector:
      name: redis-${FRONTNAME}
    type: ClusterIP
parameters:
- name: FRONTNAME
  description: The name identifier assigned to objects defined in this template
  displayName: Name
  required: true
  value: demo
- name: REDIS_CPU_LIMIT
  description: Maximum amount of CPU an Redis container can use
  displayName: Redis CPU Limit
  required: true
  value: 100m
- name: REDIS_IMAGE_TAG
  description: The ImageStreamTag we should pull images from
  displayName: Tag
  required: true
  value: master
- name: REDIS_MEMORY_LIMIT
  description: Maximum amount of memory an Redis container can use
  displayName: Redis Memory Limit
  required: true
  value: 256Mi
